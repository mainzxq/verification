//
//  ViewController.swift
//  verificationImage
//
//  Created by Mainzxq on 16/6/2.
//  Copyright © 2016年 Mainzxq. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var certificationView: UIView!
    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var successBtn: UIButton!
    @IBOutlet weak var failBtn: UIButton!
    
    let imageArray = ["kup", "kleft", "kdown", "kright"]
    let badArray = ["bup", "bleft", "bdown", "bright"]
    let goodArray = ["gup", "gleft", "gdown", "gright"]
    var picArray: Array<String> = []
    var resultArray: Array<String> = []
    var falseCount: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resetCertification()
        print(picArray)

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func refreshCertificationImages(picArray:[String]) {
        for i in 0...4 {
            let imageView = UIImageView.init(frame: CGRectMake(CGFloat(i)*40+CGFloat(i)*10, 0, 40, 40))
            let image = UIImage(named: picArray[i])
            imageView.image = image
            imageView.alpha = 0.6
            imageView.tag = 100+i
            certificationView.addSubview(imageView)
        }
    }
    
    
    @IBAction func resetCertification() {
        picArray = []
        resultArray = []
        certificationView.subviews.map{$0.removeFromSuperview()}
        picArray = generateArray()
        refreshCertificationImages(picArray)
        successBtn.hidden = true
        failBtn.hidden = true
        falseCount = 0
    }
    
    
    func generateArray() -> [String] {
        var array: Array<String> = []
        for _ in 0...4 {
            let index = arc4random() % 4
            array.append(imageArray[Int(index)])
        }
        return array
    }
    
    
    func comparePictureFromArray(btnTag: Int) {
        let myPressedCount = resultArray.count-1
        let pic = certificationView.viewWithTag(100+myPressedCount) as! UIImageView
        let index = imageArray.indexOf(picArray[myPressedCount])

        if picArray[myPressedCount] == resultArray[myPressedCount] {
            pic.image = UIImage(named:goodArray[index!])
        } else {
            pic.image = UIImage(named:badArray[index!])
            falseCount += 1
        }
        
        if resultArray.count == 5 {
            if  falseCount == 0{
                successBtn.hidden = false
            } else {
                 failBtn.hidden = false
            }
        }
    }
    
    
    @IBAction func keyButtonPressed(button: UIButton) {
        print(imageArray[button.tag])
        if resultArray.count < 5 {
            resultArray.append(imageArray[button.tag])
            comparePictureFromArray(button.tag)
        }
    }

}

